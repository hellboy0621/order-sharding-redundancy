
# 1. 同步写
写入测试数据，调用接口：http://localhost:8081/order/sync/insertTestDatas

查询同步写入数据
1. 用户维度，http://localhost:8081/order/list/user/1001
```yaml
{
  "respCode": 0,
  "respMsg": "请求成功",
  "datas": [
    {
      "orderId": 0,
      "userId": 1001,
      "merchantId": 2001,
      "totalPrice": 369.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 2,
          "orderItemUnitPrice": 96.00,
          "orderItemQuantity": 1,
          "orderItemTotalPrice": 96.00
        },
        {
          "orderItemId": 1,
          "orderItemUnitPrice": 39.00,
          "orderItemQuantity": 7,
          "orderItemTotalPrice": 273.00
        }
      ]
    },
    {
      "orderId": 1,
      "userId": 1001,
      "merchantId": 2002,
      "totalPrice": 705.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 4,
          "orderItemUnitPrice": 43.00,
          "orderItemQuantity": 11,
          "orderItemTotalPrice": 473.00
        },
        {
          "orderItemId": 3,
          "orderItemUnitPrice": 58.00,
          "orderItemQuantity": 4,
          "orderItemTotalPrice": 232.00
        }
      ]
    }
  ],
  "success": true
}
```

2. 商户维度，http://localhost:8081/order/list/merchant/2001
```yaml
{
  "respCode": 0,
  "respMsg": "请求成功",
  "datas": [
    {
      "orderId": 0,
      "userId": 1001,
      "merchantId": 2001,
      "totalPrice": 369.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 2,
          "orderItemUnitPrice": 96.00,
          "orderItemQuantity": 1,
          "orderItemTotalPrice": 96.00
        },
        {
          "orderItemId": 1,
          "orderItemUnitPrice": 39.00,
          "orderItemQuantity": 7,
          "orderItemTotalPrice": 273.00
        }
      ]
    },
    {
      "orderId": 7,
      "userId": 1004,
      "merchantId": 2001,
      "totalPrice": 1260.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 16,
          "orderItemUnitPrice": 92.00,
          "orderItemQuantity": 10,
          "orderItemTotalPrice": 920.00
        },
        {
          "orderItemId": 15,
          "orderItemUnitPrice": 68.00,
          "orderItemQuantity": 5,
          "orderItemTotalPrice": 340.00
        }
      ]
    }
  ],
  "success": true
}
```

# 2. 异步写
写入测试数据，调用接口：http://localhost:8081/order/async/insertTestDatas

查询同步写入数据
1. 用户维度，http://localhost:8081/order/list/user/10001
```yaml
{
  "respCode": 0,
  "respMsg": "请求成功",
  "datas": [
    {
      "orderId": 100,
      "userId": 10001,
      "merchantId": 20001,
      "totalPrice": 1611.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 101,
          "orderItemUnitPrice": 1.00,
          "orderItemQuantity": 9,
          "orderItemTotalPrice": 9.00
        },
        {
          "orderItemId": 100,
          "orderItemUnitPrice": 89.00,
          "orderItemQuantity": 18,
          "orderItemTotalPrice": 1602.00
        }
      ]
    },
    {
      "orderId": 101,
      "userId": 10001,
      "merchantId": 20002,
      "totalPrice": 1086.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 103,
          "orderItemUnitPrice": 78.00,
          "orderItemQuantity": 7,
          "orderItemTotalPrice": 546.00
        },
        {
          "orderItemId": 102,
          "orderItemUnitPrice": 45.00,
          "orderItemQuantity": 12,
          "orderItemTotalPrice": 540.00
        }
      ]
    }
  ],
  "success": true
}
```
2. 商户维度，http://localhost:8081/order/list/merchant/20001
```yaml
{
  "respCode": 0,
  "respMsg": "请求成功",
  "datas": [
    {
      "orderId": 100,
      "userId": 10001,
      "merchantId": 20001,
      "totalPrice": 1611.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 101,
          "orderItemUnitPrice": 1.00,
          "orderItemQuantity": 9,
          "orderItemTotalPrice": 9.00
        },
        {
          "orderItemId": 100,
          "orderItemUnitPrice": 89.00,
          "orderItemQuantity": 18,
          "orderItemTotalPrice": 1602.00
        }
      ]
    },
    {
      "orderId": 107,
      "userId": 10004,
      "merchantId": 20001,
      "totalPrice": 812.00,
      "status": 1,
      "items": [
        {
          "orderItemId": 115,
          "orderItemUnitPrice": 74.00,
          "orderItemQuantity": 1,
          "orderItemTotalPrice": 74.00
        },
        {
          "orderItemId": 114,
          "orderItemUnitPrice": 82.00,
          "orderItemQuantity": 9,
          "orderItemTotalPrice": 738.00
        }
      ]
    }
  ],
  "success": true
}
```
