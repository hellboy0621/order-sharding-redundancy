package com.demo.order.async.service.impl;

import com.demo.order.async.service.MQProducer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class RocketmqProducerImpl implements MQProducer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @Override
    public SendResult send(String topic, String msg) {
        Message<String> message = MessageBuilder.withPayload(msg).build();
        return rocketMQTemplate.syncSend(topic, message);
    }
}
