package com.demo.order.async.service;

import org.apache.rocketmq.client.producer.SendResult;

public interface MQProducer {

    SendResult send(String topic, String message);
}
