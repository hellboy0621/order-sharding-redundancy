package com.demo.order.async.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.demo.order.comm.dto.OrderAndItemDTO;
import com.demo.order.comm.mapper.OrderItemRedundantMapper;
import com.demo.order.comm.mapper.OrderRedundantMapper;
import com.demo.order.comm.po.OrderItemPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.demo.order.comm.constant.Constants.CONSUMER_GROUP_ORDER_SHARDING_SYNC;
import static com.demo.order.comm.constant.Constants.TOPIC_ORDER_SHARDING_SYNC;

/**
 * 异步写消费者，从 MQ 中读取到订单数据后，解析为对象，并落库到冗余表
 */
@Slf4j
@Service
@RocketMQMessageListener(topic = TOPIC_ORDER_SHARDING_SYNC, consumerGroup = CONSUMER_GROUP_ORDER_SHARDING_SYNC)
public class AsyncOrderConsumer implements RocketMQListener<String> {

    @Resource
    private OrderRedundantMapper orderRedundantMapper;
    @Resource
    private OrderItemRedundantMapper orderItemRedundantMapper;

    @Override
    public void onMessage(String message) {
        if (log.isDebugEnabled()) {
            log.debug("message: {}", message);
        }

        if (StrUtil.isEmpty(message)) {
            log.warn("message is empty");
            return;
        }

        OrderAndItemDTO dto;
        try {
            dto = JSONUtil.toBean(message, OrderAndItemDTO.class);
            if (dto == null) {
                log.warn("parse message to dto is null, message: {}", message);
                return;
            }
            if (dto.getOrder() == null) {
                log.warn("parse message to dto's order is null, message: {}", message);
                return;
            }
        } catch (Exception e) {
            log.error("parse message error, message: {} ", message, e);
            return;
        }

        orderRedundantMapper.insert(dto.getOrder().toOrderRedundantPO());

        if (CollectionUtil.isEmpty(dto.getOrderItems())) {
            return;
        }
        for (OrderItemPO orderItemPO : dto.getOrderItems()) {
            orderItemRedundantMapper.insert(orderItemPO.toOrderItemRedundantPO());
        }
    }
}
