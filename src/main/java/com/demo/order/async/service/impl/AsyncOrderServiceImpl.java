package com.demo.order.async.service.impl;

import cn.hutool.json.JSONUtil;
import com.demo.order.async.service.MQProducer;
import com.demo.order.comm.constant.Constants;
import com.demo.order.comm.dto.OrderAndItemDTO;
import com.demo.order.comm.mapper.OrderItemMapper;
import com.demo.order.comm.mapper.OrderMapper;
import com.demo.order.comm.po.OrderAndItemPO;
import com.demo.order.comm.po.OrderAndItemRedundantPO;
import com.demo.order.comm.po.OrderItemPO;
import com.demo.order.comm.po.OrderPO;
import com.demo.order.comm.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
@Service("asyncOrderService")
public class AsyncOrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private OrderItemMapper orderItemMapper;
    @Resource
    private MQProducer producer;

    @Override
    public List<OrderAndItemPO> listOrderAndItemsByUserId(Long userId) {
        return null;
    }

    @Override
    public List<OrderAndItemRedundantPO> listOrderAndItemsByMerchantId(Long merchantId) {
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertTestDatas() {
        // 创建测试数据
        // 4个用户分别在2个商户处下订单，每个订单有2个商品项
        // 4个用户ID，10001～10004
        // 4个商户ID，20001～20004
        long userId = 10000L;
        long merchantId = 20000L;
        int userIdDelta = 0;
        int merchantIdDelta = 2;
        Random random = new Random();
        long orderIdDelta = 100L;
        long orderItemId = 100L;
        for (int i = 0; i < 8; i++) {
            if (i % 2 == 0) {
                userIdDelta++;
                merchantIdDelta--;
            }
            OrderPO order = OrderPO.builder()
                    .id((i + orderIdDelta))
                    .userId(userId + userIdDelta)
                    .merchantId(merchantId + ((i + merchantIdDelta) % 4))
                    .status(((byte) 1))
                    .build();

            List<OrderItemPO> orderItems = new ArrayList<>();
            BigDecimal totalPrice = BigDecimal.ZERO;
            for (int j = 0; j < 2; j++) {
                BigDecimal unitPrice = BigDecimal.valueOf(Math.abs(random.nextInt(100)));
                int quantity = Math.abs(random.nextInt(20));
                BigDecimal eachItemTotalPrice = unitPrice.multiply(BigDecimal.valueOf(quantity));
                OrderItemPO orderItem = OrderItemPO.builder()
                        .id(orderItemId++)
                        .orderId(order.getId())
                        .userId(order.getUserId())
                        .merchantId(order.getMerchantId())
                        .unitPrice(unitPrice)
                        .quantity(quantity)
                        .totalPrice(eachItemTotalPrice)
                        .build();
                totalPrice = totalPrice.add(eachItemTotalPrice);
                orderItemMapper.insert(orderItem);
                orderItems.add(orderItem);
            }

            order.setTotalPrice(totalPrice);
            orderMapper.insert(order);

            // 异步写，以订单的维度发送消息
            OrderAndItemDTO dto = new OrderAndItemDTO(order, orderItems);
            SendResult sendResult = producer.send(Constants.TOPIC_ORDER_SHARDING_SYNC, JSONUtil.toJsonStr(dto));
            log.info("sendResult: {}", JSONUtil.toJsonStr(sendResult));
        }
    }
}
