package com.demo.order.sync.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.demo.order.comm.mapper.OrderItemMapper;
import com.demo.order.comm.mapper.OrderItemRedundantMapper;
import com.demo.order.comm.mapper.OrderMapper;
import com.demo.order.comm.mapper.OrderRedundantMapper;
import com.demo.order.comm.po.*;
import com.demo.order.comm.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Slf4j
@Service("syncOrderService")
public class SyncOrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;
    @Resource
    private OrderItemMapper orderItemMapper;
    @Resource
    private OrderRedundantMapper orderRedundantMapper;
    @Resource
    private OrderItemRedundantMapper orderItemRedundantMapper;

    @Override
    public List<OrderAndItemPO> listOrderAndItemsByUserId(Long userId) {
        if (userId == null) {
            return Collections.emptyList();
        }

        return orderMapper.listOrderAndItemsByUserId(userId);
    }

    @Override
    public List<OrderAndItemRedundantPO> listOrderAndItemsByMerchantId(Long merchantId) {
        if (merchantId == null) {
            return Collections.emptyList();
        }

        return orderRedundantMapper.listOrderAndItemsByMerchantId(merchantId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertTestDatas() {
        // 创建测试数据
        // 4个用户分别在2个商户处下订单，每个订单有2个商品项
        // 4个用户ID，1001～1004
        // 4个商户ID，2001～2004
        long userId = 1000L;
        long merchantId = 2000L;
        int userIdDelta = 0;
        int merchantIdDelta = 2;
        Random random = new Random();
        long orderItemId = 1L;
        for (int i = 0; i < 8; i++) {
            if (i % 2 == 0) {
                userIdDelta++;
                merchantIdDelta--;
            }
            OrderPO order = OrderPO.builder()
                    .id(((long) i))
                    .userId(userId + userIdDelta)
                    .merchantId(merchantId + ((i + merchantIdDelta) % 4))
                    .status(((byte) 1))
                    .build();

            BigDecimal totalPrice = BigDecimal.ZERO;
            for (int j = 0; j < 2; j++) {
                BigDecimal unitPrice = BigDecimal.valueOf(Math.abs(random.nextInt(100)));
                int quantity = Math.abs(random.nextInt(20));
                BigDecimal eachItemTotalPrice = unitPrice.multiply(BigDecimal.valueOf(quantity));
                OrderItemPO orderItem = OrderItemPO.builder()
                        .id(orderItemId++)
                        .orderId((long) i)
                        .userId(order.getUserId())
                        .merchantId(order.getMerchantId())
                        .unitPrice(unitPrice)
                        .quantity(quantity)
                        .totalPrice(eachItemTotalPrice)
                        .build();
                totalPrice = totalPrice.add(eachItemTotalPrice);
                orderItemMapper.insert(orderItem);

                // 同步写
                OrderItemRedundantPO orderItemRedundantPO = new OrderItemRedundantPO();
                BeanUtil.copyProperties(orderItem, orderItemRedundantPO);
                orderItemRedundantMapper.insert(orderItemRedundantPO);
            }

            order.setTotalPrice(totalPrice);
            orderMapper.insert(order);

            // 同步写
            OrderRedundantPO orderRedundantPO = new OrderRedundantPO();
            BeanUtil.copyProperties(order, orderRedundantPO);
            orderRedundantMapper.insert(orderRedundantPO);
        }
    }
}
