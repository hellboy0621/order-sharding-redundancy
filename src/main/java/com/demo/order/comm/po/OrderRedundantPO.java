package com.demo.order.comm.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@TableName("tb_order_redundant")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderRedundantPO {

    private Long id;
    private Long userId;
    private Long merchantId;
    private BigDecimal totalPrice;
    private Byte status;
}
