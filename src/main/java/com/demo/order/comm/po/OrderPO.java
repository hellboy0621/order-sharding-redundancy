package com.demo.order.comm.po;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@TableName("tb_order")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderPO {

    private Long id;
    private Long userId;
    private Long merchantId;
    private BigDecimal totalPrice;
    private Byte status;

    public OrderRedundantPO toOrderRedundantPO() {
        OrderRedundantPO po = new OrderRedundantPO();
        BeanUtil.copyProperties(this, po);
        return po;
    }
}
