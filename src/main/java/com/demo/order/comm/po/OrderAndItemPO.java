package com.demo.order.comm.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class OrderAndItemPO extends OrderPO {

    private Long orderItemId;
    private BigDecimal orderItemUnitPrice;
    private Integer orderItemQuantity;
    private BigDecimal orderItemTotalPrice;
}
