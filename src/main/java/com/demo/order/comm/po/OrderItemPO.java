package com.demo.order.comm.po;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@TableName("tb_order_item")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemPO {

    private Long id;
    private Long orderId;
    private Long userId;
    private Long merchantId;
    private BigDecimal unitPrice;
    private Integer quantity;
    private BigDecimal totalPrice;

    public OrderItemRedundantPO toOrderItemRedundantPO() {
        OrderItemRedundantPO po = new OrderItemRedundantPO();
        BeanUtil.copyProperties(this, po);
        return po;
    }
}
