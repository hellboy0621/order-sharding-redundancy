package com.demo.order.comm.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@TableName("tb_order_item_redundant")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemRedundantPO {

    private Long id;
    private Long orderId;
    private Long userId;
    private Long merchantId;
    private BigDecimal unitPrice;
    private Integer quantity;
    private BigDecimal totalPrice;
}
