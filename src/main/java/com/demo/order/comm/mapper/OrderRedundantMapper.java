package com.demo.order.comm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.order.comm.po.OrderAndItemRedundantPO;
import com.demo.order.comm.po.OrderRedundantPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderRedundantMapper extends BaseMapper<OrderRedundantPO> {

    List<OrderAndItemRedundantPO> listOrderAndItemsByMerchantId(@Param("merchantId") Long merchantId);

}
