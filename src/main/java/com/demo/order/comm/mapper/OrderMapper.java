package com.demo.order.comm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.order.comm.po.OrderAndItemPO;
import com.demo.order.comm.po.OrderPO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper extends BaseMapper<OrderPO> {

    List<OrderAndItemPO> listOrderAndItemsByUserId(@Param("userId") Long userId);
}
