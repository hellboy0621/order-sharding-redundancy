package com.demo.order.comm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.order.comm.po.OrderItemPO;

public interface OrderItemMapper extends BaseMapper<OrderItemPO> {

}
