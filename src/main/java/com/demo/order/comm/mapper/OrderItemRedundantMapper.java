package com.demo.order.comm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.order.comm.po.OrderItemRedundantPO;

public interface OrderItemRedundantMapper extends BaseMapper<OrderItemRedundantPO> {

}
