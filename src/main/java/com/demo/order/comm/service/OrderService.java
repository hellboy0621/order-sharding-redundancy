package com.demo.order.comm.service;

import com.demo.order.comm.po.OrderAndItemPO;
import com.demo.order.comm.po.OrderAndItemRedundantPO;

import java.util.List;

public interface OrderService {

    List<OrderAndItemPO> listOrderAndItemsByUserId(Long userId);

    List<OrderAndItemRedundantPO> listOrderAndItemsByMerchantId(Long merchantId);

    void insertTestDatas();
}
