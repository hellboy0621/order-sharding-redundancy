package com.demo.order.comm.vo;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
public class OrderVO {

    private Long orderId;
    private Long userId;
    private Long merchantId;
    private BigDecimal totalPrice;
    private Byte status;

    private List<OrderItemVO> items;

}
