package com.demo.order.comm.vo;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class OrderItemVO {

    private Long orderItemId;
    private BigDecimal orderItemUnitPrice;
    private Integer orderItemQuantity;
    private BigDecimal orderItemTotalPrice;

}
