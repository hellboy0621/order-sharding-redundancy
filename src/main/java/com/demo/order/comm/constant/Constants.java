package com.demo.order.comm.constant;

public interface Constants {

    /**
     * 订单分片同步 topic
     */
    String TOPIC_ORDER_SHARDING_SYNC = "ORDER_SHARDING_SYNC";
    /**
     * 消费者组名称
     */
    String CONSUMER_GROUP_ORDER_SHARDING_SYNC = "order-sharding-redundancy";
}
