package com.demo.order.comm.controller;

import cn.hutool.core.bean.BeanUtil;
import com.demo.order.comm.po.OrderAndItemPO;
import com.demo.order.comm.po.OrderAndItemRedundantPO;
import com.demo.order.comm.result.RestOut;
import com.demo.order.comm.service.OrderService;
import com.demo.order.comm.vo.OrderItemVO;
import com.demo.order.comm.vo.OrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource(name = "syncOrderService")
    private OrderService syncOrderService;
    @Resource(name = "asyncOrderService")
    private OrderService asyncOrderService;

    @GetMapping("/list/user/{userId}")
    public RestOut<List<OrderVO>> listOrderAndItemsByUserId(@PathVariable("userId") Long userId) {
        List<OrderAndItemPO> orderAndItemPOs = syncOrderService.listOrderAndItemsByUserId(userId);
        Map<Long, List<OrderAndItemPO>> orderIdMapping = orderAndItemPOs.stream()
                .collect(Collectors.groupingBy(OrderAndItemPO::getId));
        List<OrderVO> result = new ArrayList<>();
        for (List<OrderAndItemPO> pos : orderIdMapping.values()) {
            OrderAndItemPO orderAndItemPO = pos.get(0);
            OrderVO orderVO = OrderVO.builder()
                    .orderId(orderAndItemPO.getId())
                    .build();
            BeanUtil.copyProperties(orderAndItemPO, orderVO);
            orderVO.setItems(new ArrayList<>());
            result.add(orderVO);

            for (OrderAndItemPO po : pos) {
                OrderItemVO orderItemVO = OrderItemVO.builder().build();
                BeanUtil.copyProperties(po, orderItemVO);
                orderVO.getItems().add(orderItemVO);
            }
        }
        return RestOut.success(result);
    }

    @GetMapping("/list/merchant/{merchantId}")
    public RestOut<List<OrderVO>> listOrderAndItemsByMerchantId(@PathVariable("merchantId") Long merchantId) {
        List<OrderAndItemRedundantPO> orderAndItemRedundantPOs = syncOrderService.listOrderAndItemsByMerchantId(merchantId);
        Map<Long, List<OrderAndItemRedundantPO>> orderIdMapping = orderAndItemRedundantPOs.stream()
                .collect(Collectors.groupingBy(OrderAndItemRedundantPO::getId));
        List<OrderVO> result = new ArrayList<>();
        for (List<OrderAndItemRedundantPO> pos : orderIdMapping.values()) {
            OrderAndItemRedundantPO orderAndItemPO = pos.get(0);
            OrderVO orderVO = OrderVO.builder()
                    .orderId(orderAndItemPO.getId())
                    .build();
            BeanUtil.copyProperties(orderAndItemPO, orderVO);
            orderVO.setItems(new ArrayList<>());
            result.add(orderVO);

            for (OrderAndItemRedundantPO po : pos) {
                OrderItemVO orderItemVO = OrderItemVO.builder().build();
                BeanUtil.copyProperties(po, orderItemVO);
                orderVO.getItems().add(orderItemVO);
            }
        }
        return RestOut.success(result);
    }

    @GetMapping("/sync/insertTestDatas")
    public RestOut<?> syncInsertTestDatas() {
        syncOrderService.insertTestDatas();
        return RestOut.succeed("sync create test data success.");
    }

    @GetMapping("/async/insertTestDatas")
    public RestOut<?> asyncInsertTestDatas() {
        asyncOrderService.insertTestDatas();
        return RestOut.succeed("async create test data success.");
    }
}
