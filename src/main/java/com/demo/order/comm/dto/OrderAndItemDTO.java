package com.demo.order.comm.dto;

import com.demo.order.comm.po.OrderItemPO;
import com.demo.order.comm.po.OrderPO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderAndItemDTO {

    private OrderPO order;
    private List<OrderItemPO> orderItems;
}
