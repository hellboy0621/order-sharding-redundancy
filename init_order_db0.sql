-- 创建数据库 指定字符集
CREATE DATABASE `order_db0` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

use order_db0;

DROP TABLE IF EXISTS `tb_order_0`;
CREATE TABLE `tb_order_0`
(
    `id`          bigint         NOT NULL,
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `total_price` decimal(10, 2) NULL DEFAULT NULL comment '订单总价',
    `status`      tinyint        NULL DEFAULT NULL comment '订单状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC comment '订单表';
DROP TABLE IF EXISTS `tb_order_1`;
CREATE TABLE `tb_order_1`
(
    `id`          bigint         NOT NULL,
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `total_price` decimal(10, 2) NULL DEFAULT NULL comment '订单总价',
    `status`      tinyint        NULL DEFAULT NULL comment '订单状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC comment '订单表';
DROP TABLE IF EXISTS `tb_order_2`;
CREATE TABLE `tb_order_2`
(
    `id`          bigint         NOT NULL,
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `total_price` decimal(10, 2) NULL DEFAULT NULL comment '订单总价',
    `status`      tinyint        NULL DEFAULT NULL comment '订单状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC comment '订单表';
DROP TABLE IF EXISTS `tb_order_3`;
CREATE TABLE `tb_order_3`
(
    `id`          bigint         NOT NULL,
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `total_price` decimal(10, 2) NULL DEFAULT NULL comment '订单总价',
    `status`      tinyint        NULL DEFAULT NULL comment '订单状态',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC comment '订单表';


DROP TABLE IF EXISTS `tb_order_item_0`;
CREATE TABLE `tb_order_item_0`
(
    `id`          bigint         NOT NULL,
    `order_id`    bigint         NULL DEFAULT NULL comment '订单ID',
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `unit_price`   decimal(10, 2) NULL DEFAULT NULL comment '单价',
    `quantity`    bigint         NULL DEFAULT NULL comment '数量',
    `total_price`  decimal(10, 2) NULL DEFAULT NULL comment '总价',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic comment '订单条目表';
DROP TABLE IF EXISTS `tb_order_item_1`;
CREATE TABLE `tb_order_item_1`
(
    `id`          bigint         NOT NULL,
    `order_id`    bigint         NULL DEFAULT NULL comment '订单ID',
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `unit_price`   decimal(10, 2) NULL DEFAULT NULL comment '单价',
    `quantity`    bigint         NULL DEFAULT NULL comment '数量',
    `total_price`  decimal(10, 2) NULL DEFAULT NULL comment '总价',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic comment '订单条目表';
DROP TABLE IF EXISTS `tb_order_item_2`;
CREATE TABLE `tb_order_item_2`
(
    `id`          bigint         NOT NULL,
    `order_id`    bigint         NULL DEFAULT NULL comment '订单ID',
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `unit_price`   decimal(10, 2) NULL DEFAULT NULL comment '单价',
    `quantity`    bigint         NULL DEFAULT NULL comment '数量',
    `total_price`  decimal(10, 2) NULL DEFAULT NULL comment '总价',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic comment '订单条目表';
DROP TABLE IF EXISTS `tb_order_item_3`;
CREATE TABLE `tb_order_item_3`
(
    `id`          bigint         NOT NULL,
    `order_id`    bigint         NULL DEFAULT NULL comment '订单ID',
    `user_id`     bigint         NULL DEFAULT NULL comment '用户ID',
    `merchant_id` bigint         NULL DEFAULT NULL comment '商户ID',
    `unit_price`   decimal(10, 2) NULL DEFAULT NULL comment '单价',
    `quantity`    bigint         NULL DEFAULT NULL comment '数量',
    `total_price`  decimal(10, 2) NULL DEFAULT NULL comment '总价',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic comment '订单条目表';
